# Public, Spectators Read-Only

This is a public repo to which Spectators should only have read access (`member`).
